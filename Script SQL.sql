CREATE TABLE IF NOT EXISTS Materia
(
    "Id_Materia" integer NOT NULL ,
    "Nombre" character varying(50) ,
    CONSTRAINT "Materia_pkey" PRIMARY KEY ("Id_Materia")
)


CREATE TABLE IF NOT EXISTS Grado
(
    "ID_Grado" integer NOT NULL,
    "Nombre" character varying(50) COLLATE pg_catalog."default",
    CONSTRAINT "Grado_pkey" PRIMARY KEY ("ID_Grado")
)


CREATE TABLE Nota
(
    "Id_Nota" serial NOT NULL,
    "Literal" "char" NOT NULL,
    "Rango_desde" integer NOT NULL,
    "Rango_hasta" integer NOT NULL,
    PRIMARY KEY ("Id_Nota")
);

CREATE TABLE IF NOT EXISTS Estudiante
(
    "Id_estudiante" integer NOT NULL ,
    "Nombre" character varying(50) COLLATE pg_catalog."default" NOT NULL,
    "Apellido" character varying(50) COLLATE pg_catalog."default" NOT NULL,
    "Matricula" character varying(20) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT "Estudiante_pkey" PRIMARY KEY ("Id_estudiante"),
    CONSTRAINT "Matricula_uniqueKey" UNIQUE ("Matricula")
)

CREATE TABLE Estudiante_En_Curso
(
    "Id_Estudiante_en_curso" serial NOT NULL,
    "Id_Estudiante" integer NOT NULL,
    "Id_Grado" integer NULL,
    PRIMARY KEY ("Id_Estudiante_en_curso"),
    UNIQUE ("Id_Estudiante", "Id_Grado"),
    CONSTRAINT "FK_Estudiante_Curso_Estudiante" FOREIGN KEY ("Id_Estudiante")
        REFERENCES Estudiante ("Id_estudiante") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT "FK_Estudiante_Curso_Grado" FOREIGN KEY ("Id_Grado")
        REFERENCES Grado ("ID_Grado") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);



CREATE TABLE Materia_Estudiante_Grado
(
    "Id_Materia_Estudiante_Grado" serial NOT NULL,
    "Id_Estudiante_Curso" integer NOT NULL,
    "Id_Materia" integer NOT NULL,
    PRIMARY KEY ("Id_Materia_Estudiante_Grado"),
    CONSTRAINT "MEG_uniqueKey" UNIQUE ("Id_Estudiante_Curso", "Id_Materia"),
    CONSTRAINT "FK_MEG_Estudiante_Curso" FOREIGN KEY ("Id_Estudiante_Curso")
        REFERENCES Estudiante_En_Curso ("Id_Estudiante_en_curso") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT "FK_MEG_Materia" FOREIGN KEY ("Id_Materia")
        REFERENCES Materia ("Id_Materia") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);


CREATE TABLE Calificacion
(
    "Id_Calificacion" serial NOT NULL,
    "Id_Estudiante_Grado_Materia" integer NOT NULL,
    "Id_Materia" integer NOT NULL,
    "Nota" integer NOT NULL,
    PRIMARY KEY ("Id_Calificacion"),
    CONSTRAINT "Calificacion_Unique" UNIQUE ("Id_Estudiante_Grado_Materia", "Id_Materia"),
    CONSTRAINT "Fk_Calificacion_MEG" FOREIGN KEY ("Id_Estudiante_Grado_Materia")
        REFERENCES Materia_Estudiante_Grado ("Id_Materia_Estudiante_Grado") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT "FK_Calificacion_Materia" FOREIGN KEY ("Id_Materia")
        REFERENCES Materia ("Id_Materia") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);

CREATE TABLE Asistencia
(
    "Id_Asistencia" serial NOT NULL,
    "Id_Materia_Estudiante_Grado" integer NOT NULL,
    "Fecha" date NOT NULL,
    "Asistencia" boolean NOT NULL,
    PRIMARY KEY ("Id_Asistencia"),
    CONSTRAINT "FK_Asistencia_MEG" FOREIGN KEY ("Id_Materia_Estudiante_Grado")
        REFERENCES Materia_Estudiante_Grado ("Id_Materia_Estudiante_Grado") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);