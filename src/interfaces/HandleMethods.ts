export type HandleMethods = {
    url: string;
    handleChange: ({target}: {target:any}) => void;
    handleSubmit: (e:any) => void;
}