export type AsyncGetUpdateDeleteInterface = {
    asyncGetMethod: () => Promise<void>
    asyncUpdateMethod: (data:any) => Promise<void>;
    asyncDeleteMethod: (data:any) => Promise<void>;
    showModal: (data:any) => void;
    loadDataMethod:(data:any) => void;
    manageActions: (e:any) => void;
}

