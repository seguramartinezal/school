import React from "react";

class Checkbox extends React.Component<any, any> {
    constructor(props:any) {
        super(props);
        this.state = {
            isChecked: false
        };
    }
    toggleChange = () => {
        this.setState({
            isChecked: !this.state.isChecked
        });
        console.log(this.state.isChecked);
    }

    toggleChange2 = (data: any) => {
        this.setState({
            isChecked: !data
        });
        console.log(this.state.isChecked);
    }

    render() {
        return (
            <label>
                <input type="checkbox"
                       defaultChecked={this.state.isChecked}
                       onChange={() => this.toggleChange2(this.state.isChecked)}
                />
            </label>
        );
    }
}


export default Checkbox;

