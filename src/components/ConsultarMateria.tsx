import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../styles/ConsultarMateria.css";
import {AsyncGetUpdateDeleteInterface} from "../interfaces/AsyncGetUpdateDeleteInterface";
import {HandleMethods} from "../interfaces/HandleMethods";
import axios from "axios";
import ModalMateria from "./ModalMateria";
import {toast, ToastContainer} from "react-toastify";


class ConsultarMateria extends React.Component<any, any> implements AsyncGetUpdateDeleteInterface, HandleMethods{

    constructor(props:any) {
        super(props);
        this.state = {
            nombreMateriaAct: String,
            AllSubjects: [],
            modal: false,
            obj: {},
            AllSubjectsCopy: []
        }

        this.showModal = this.showModal.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.asyncGetMethod = this.asyncGetMethod.bind(this);
        this.asyncDeleteMethod = this.asyncDeleteMethod.bind(this);
        this.manageActions = this.manageActions.bind(this);
        this.asyncUpdateMethod = this.asyncUpdateMethod.bind(this);
    }

    url: string = "http://localhost:4000/";

    componentDidMount() {
        this.asyncGetMethod();
    }

    handleChange({target}: { target: any }): void {
        const {name, value} = target;
        this.setState(() => ({[name]:value}));
    }

    handleSubmit(e: any): void {
        e.preventDefault();
    }

    async asyncGetMethod(): Promise<void> {
        let dataArray: any[] = [];
        const res = await axios.get(this.url+"ConsultarMateria");

        for (let i = 0; i < res.data.length; i++) {

            const payload = {
                id: res.data[i].Id_Materia,
                nombre: res.data[i].Nombre
            }

           dataArray.push(payload)

           // this.setState((prevState:any) => ({AllSubjects:[...prevState.AllSubjects, payload]}))
        }

        this.setState(() => ({AllSubjects:dataArray}));
        this.setState(() => ({AllSubjectsCopy:dataArray}));

    }

    async asyncDeleteMethod(data: any): Promise<void> {
        const res = await axios.delete(this.url+"ConsultarMateria", {data: {data: data.data.id}});

        const jsonResponse = res.data;
        if(jsonResponse.success){
            const studentArray = this.state.AllSubjects;
            const newArray = studentArray.filter((item:any) => item.id != data.data.id);
            this.setState(() => ({AllSubjects:newArray}))
            this.setState(() => ({AllSubjectsCopy:newArray}));
        }

        this.notificacionBorrar(jsonResponse);
    }


   async asyncUpdateMethod(data: any): Promise<void> {

        const payload = {
            id: data.data.id,
            nombre: this.state.nombreMateriaAct
        }

        const res = await axios.put(this.url+"ConsultarMateria", {data: payload});

       const jsonResponse = res.data;

       if(jsonResponse.success){
           const scoreArray = this.state.AllSubjects;
           const newArray = scoreArray.filter((item:any) => item.id != payload.id);
           newArray.push(payload);
           this.setState(() => ({AllSubjects:newArray}))
           this.setState(() => ({AllSubjectsCopy:newArray}));
       }

       this.notificacionActualizar(jsonResponse);

    }

    filterData = ({target}: {target:any}) => {
        const valor = target.value;

        const arrayCopy = this.state.AllSubjectsCopy;
        const ans = arrayCopy.filter((item:any) => valor.toUpperCase() === item.nombre.toUpperCase());
        this.setState(() => ({AllSubjects:ans}));
        if(valor.length == 0 ){
            this.setState(() => ({AllSubjects:arrayCopy}));
        }

    }

    loadDataMethod(data: any): void {

        this.setState(() => ({nombreMateriaAct:data.nombre, obj:{data}}))
    }

    manageActions(e: any): void {
        this.showModal(e);

        if(e.target.value === "deleteSubject"){
            this.asyncDeleteMethod(this.state.obj)
        }

        if(e.target.value === "updateSubject"){
            this.asyncUpdateMethod(this.state.obj)
        }
    }

    showModal(data: any): void {
        this.loadDataMethod(data);
        this.setState(() => ({modal: !this.state.modal}))
    }

    notificacionActualizar(data:any){

        switch (data.success) {

            case true: {
                return toast.success("Registro actualizado", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                })
            }

            case false:{
                return toast.error("Error", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                })
            }

            default:
                break;

        }
    }

    notificacionBorrar(data:any){

        switch (data.success) {

            case true: {
                return toast.success("Registro borrado", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                })
            }

            case false:{
                return toast.error("Error", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                })
            }

            default:
                break;

        }
    }





    render() {
        return (
            <div>
                <ToastContainer theme={"dark"}/>
                <ModalMateria state={this.state} toggle={this.showModal} modal={this.state.modal}
                                 handleChange={this.handleChange} handleSubmit={this.handleSubmit}
                                 handleActions={this.manageActions}/>

                <form className={"form"}>

                    <div className={"form-floating"} id={"BarraFiltroMateria"}>
                        <input type={"text"} placeholder={"Ingrese la materia"} name={"filtrarMateria"} onKeyUp={this.filterData}
                               className={"form-control"} autoComplete={"off"}/><br/>
                        <label htmlFor={"Filtro"}><i>Filtrar materia</i></label>
                    </div>
                </form>

                <table className={"table table-bordered"}>
                    <thead>
                    <tr>
                        <th scope={"col"}>#</th>
                        <th scope={"col"}>Nombre</th>

                    </tr>
                    </thead>

                    <tbody>

                    {this.state.AllSubjects.map((item:any, index:number) =>

                        <tr key={index} onClick={() => this.showModal(item)}>
                            <th scope={"row"}>{item.id}</th>
                            <td>{item.nombre}</td>
                        </tr>

                    )}

                    </tbody>

                </table>

            </div>
        );
    }

}

export default ConsultarMateria;