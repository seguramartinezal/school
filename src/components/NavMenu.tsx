import React from "react";
import {Link} from "react-router-dom";
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem } from 'reactstrap';
import "../styles/NavMenu.css";

class NavMenu extends React.Component<any, any>{

    constructor (props:any) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            collapsed: true
        };
    }

    toggle () {
        this.setState({
            collapsed: !this.state.collapsed
        });

    }


    render() {
        return (
            <>
                <header>
                    <Navbar color="light" light expand="md">
                        <NavbarBrand tag={Link} to={"/"}>Inicio</NavbarBrand>
                        <NavbarToggler onClick={this.toggle} />
                        <Collapse isOpen={this.state.isOpen} navbar>
                            <Nav className="mx-auto" navbar>

                                <UncontrolledDropdown nav inNavbar>
                                    <DropdownToggle nav caret>
                                        Estudiantes
                                    </DropdownToggle>
                                    <DropdownMenu end>
                                        <DropdownItem>
                                            <NavLink tag={Link} to={"/RegistrarEstudiantes"}>Registrar</NavLink>
                                        </DropdownItem>
                                        <DropdownItem>
                                            <NavLink tag={Link} to={"/ConsultarEstudiantes"}>Consultar</NavLink>
                                        </DropdownItem>
                                    </DropdownMenu>
                                </UncontrolledDropdown>

                             {/*   <UncontrolledDropdown nav inNavbar>
                                    <DropdownToggle nav caret>
                                        Materia
                                    </DropdownToggle>
                                    <DropdownMenu end>
                                        <DropdownItem>
                                            <NavLink tag={Link} to={"/RegistrarMateria"}>Registrar</NavLink>
                                        </DropdownItem>
                                        <DropdownItem>
                                            <NavLink tag={Link} to={"/ConsultarMateria"}>Consultar</NavLink>
                                        </DropdownItem>
                                    </DropdownMenu>
                                </UncontrolledDropdown>*/}

                                <UncontrolledDropdown nav inNavbar>
                                    <DropdownToggle nav caret>
                                        Curso
                                    </DropdownToggle>
                                    <DropdownMenu end>
                                        <DropdownItem>
                                            <NavLink tag={Link} to={"/RegistrarGrado"}>Registrar</NavLink>
                                        </DropdownItem>
                                        <DropdownItem>
                                            <NavLink tag={Link} to={"/ConsultarGrado"}>Consultar</NavLink>
                                        </DropdownItem>
                                    </DropdownMenu>
                                </UncontrolledDropdown>

                               {/* <UncontrolledDropdown nav inNavbar>
                                    <DropdownToggle nav caret>
                                        Nota
                                    </DropdownToggle>
                                    <DropdownMenu end>
                                        <DropdownItem>
                                            <NavLink tag={Link} to={"/RegistrarNota"}>Registrar</NavLink>

                                        </DropdownItem>
                                        <DropdownItem>
                                            <NavLink tag={Link} to={"/ConsultarNota"}>Consultar</NavLink>
                                        </DropdownItem>
                                    </DropdownMenu>
                                </UncontrolledDropdown>*/}

                                <UncontrolledDropdown nav inNavbar>
                                    <DropdownToggle nav caret>
                                        Inscripcion estudiante
                                    </DropdownToggle>
                                    <DropdownMenu end>
                                        <DropdownItem>
                                            <NavLink tag={Link} to={"/Inscripcion"}>Curso</NavLink>

                                        </DropdownItem>
                                    </DropdownMenu>
                                </UncontrolledDropdown>


                                <UncontrolledDropdown nav inNavbar>
                                    <DropdownToggle nav caret>
                                        Calificacion
                                    </DropdownToggle>
                                    <DropdownMenu end>
                                        <DropdownItem>
                                            <NavLink tag={Link} to={"/Calificacion"}>Registrar</NavLink>

                                        </DropdownItem>
                                    </DropdownMenu>
                                </UncontrolledDropdown>



                                <UncontrolledDropdown nav inNavbar>
                                    <DropdownToggle nav caret>
                                        Asistencia
                                    </DropdownToggle>
                                    <DropdownMenu end>
                                        <DropdownItem>
                                            <NavLink tag={Link} to={"/Asistencia"}>Registro</NavLink>

                                        </DropdownItem>

                                        <DropdownItem>
                                            <NavLink tag={Link} to={"/HistorialAsistencia"}>Historial</NavLink>

                                        </DropdownItem>
                                    </DropdownMenu>

                                </UncontrolledDropdown>

                               {/* <UncontrolledDropdown nav inNavbar>
                                    <DropdownToggle nav caret>
                                        Historial
                                    </DropdownToggle>
                                    <DropdownMenu end>
                                        <DropdownItem>
                                            <NavLink tag={Link} to={"/HistorialAsistencia"}>Ver</NavLink>

                                        </DropdownItem>
                                    </DropdownMenu>
                                </UncontrolledDropdown>*/}


                            </Nav>
                        </Collapse>
                    </Navbar>
                </header>
            </>
        )
    }
}

export default NavMenu;