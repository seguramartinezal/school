import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../styles/ConsultarCurso.css";
import {HandleMethods} from "../interfaces/HandleMethods";
import axios from "axios";
import {AsyncGetUpdateDeleteInterface} from "../interfaces/AsyncGetUpdateDeleteInterface";
import ModalCurso from "./ModalCurso";
import {toast, ToastContainer} from "react-toastify";


class ConsultarCurso extends React.Component<any, any> implements AsyncGetUpdateDeleteInterface, HandleMethods{

    constructor(props:any) {
        super(props);
        this.state = {
            nombreGradoAct: String,
            AllGrades: [],
            modal: false,
            obj: {},
            AllGradesCopy: []
        }

        this.showModal = this.showModal.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.asyncGetMethod = this.asyncGetMethod.bind(this);
        this.asyncDeleteMethod = this.asyncDeleteMethod.bind(this);
        this.manageActions = this.manageActions.bind(this);
        this.asyncUpdateMethod = this.asyncUpdateMethod.bind(this);
    }

    url: string = "http://localhost:4000/";

    componentDidMount() {
        this.asyncGetMethod();
    }

    handleChange({target}: { target: any }): void {
        const {name, value} = target;
        this.setState(() => ({[name]:value}));
    }

    handleSubmit(e: any): void {
        e.preventDefault();
    }

    async asyncGetMethod(): Promise<void> {
        let dataArray: any[] = [];
        const res = await axios.get(this.url+"ConsultarGrado");

        for (let i = 0; i < res.data.length; i++) {

        const payload = {
            id: res.data[i].ID_Grado,
            nombre: res.data[i].Nombre
        }
        dataArray.push(payload);

       // this.setState((prevState:any) => ({AllGrades:[...prevState.AllGrades, payload]}))

        }

        this.setState(() => ({AllGrades:dataArray}));
        this.setState(() => ({AllGradesCopy:dataArray}));

}

    async asyncDeleteMethod(data: any): Promise<void> {
        const res = await axios.delete(this.url+"ConsultarGrado", {data: {data: data.data.id}});
        const jsonResponse = res.data;
        if(jsonResponse.success){
            const studentArray = this.state.AllGrades;
            const newArray = studentArray.filter((item:any) => item.id != data.data.id);
            this.setState(() => ({AllGrades:newArray}))
            this.setState(() => ({AllGradesCopy:newArray}));
        }

        this.notificacionBorrar(jsonResponse);
    }


    async asyncUpdateMethod(data: any): Promise<void> {

        const payload = {
            id: data.data.id,
            nombre: this.state.nombreGradoAct
        }

        const res = await axios.put(this.url+"ConsultarGrado", {data: payload});
        const jsonResponse = res.data;

        if(jsonResponse.success){
            const scoreArray = this.state.AllGrades;
            const newArray = scoreArray.filter((item:any) => item.id != payload.id);
            newArray.push(payload);
            this.setState(() => ({AllGrades:newArray}))
            this.setState(() => ({AllGradesCopy:newArray}));
        }

        this.notificacionActualizar(jsonResponse);

    }

    filterData = ({target}: {target:any}) => {
        const valor = target.value;
        const arrayCopy = this.state.AllGradesCopy;
        const ans = arrayCopy.filter((item:any) => valor.toUpperCase() === item.nombre.toUpperCase());
        this.setState(() => ({AllGrades:ans}));
        if(valor.length == 0 ){
            this.setState(() => ({AllGrades:arrayCopy}));
        }

    }



    loadDataMethod(data: any): void {

        this.setState(() => ({nombreGradoAct:data.nombre, obj:{data}}))
    }

    manageActions(e: any): void {
        this.showModal(e);

        if(e.target.value === "deleteGrade"){
        this.asyncDeleteMethod(this.state.obj)
    }

    if(e.target.value === "updateGrade"){
        this.asyncUpdateMethod(this.state.obj)
    }
}

    showModal(data: any): void {
        this.loadDataMethod(data);
        this.setState(() => ({modal: !this.state.modal}))
    }

    notificacionActualizar(data:any){

        switch (data.success) {

            case true: {
                return toast.success("Registro actualizado", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                })
            }

            case false:{
                return toast.error("Error", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                })
            }

            default:
                break;

        }
    }

    notificacionBorrar(data:any){

        switch (data.success) {

            case true: {
                return toast.success("Registro borrado", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                })
            }

            case false:{
                return toast.error("Error", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                })
            }

            default:
                break;

        }
    }

    render() {
        return (
            <div>
                <ToastContainer theme={"dark"}/>

                <h3 style={{position: "relative", left:"16em", top:"2em", width: "10em"}}>Consultar Curso</h3>
                <ModalCurso state={this.state} toggle={this.showModal} modal={this.state.modal}
                              handleChange={this.handleChange} handleSubmit={this.handleSubmit}
                              handleActions={this.manageActions}/>

                <form className={"formCurso"}>

                    <div className={"form-floating"} id={"BarraFiltroCurso"}>
                        <input type={"text"} placeholder={"Ingrese el curso"} name={"filtrarCurso"} onKeyUp={this.filterData}
                               className={"form-control"} autoComplete={"off"}/><br/>
                        <label htmlFor={"Filtro"}><i>Filtrar grado</i></label>
                    </div>
                </form>

                <table className={"table table-bordered"}>
                    <thead>
                    <tr>
                        <th scope={"col"}>#</th>
                        <th scope={"col"}>Nombre</th>

                    </tr>
                    </thead>

                    <tbody>

                    {this.state.AllGrades.map((item:any, index:number) =>

                        <tr key={index} onClick={() => this.showModal(item)}>
                            <th scope={"row"}>{item.id}</th>
                            <td>{item.nombre}</td>
                        </tr>

                    )}

                    </tbody>
                </table>
            </div>


        );
    }
}

export default ConsultarCurso;