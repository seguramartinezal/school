import React, {ChangeEventHandler, FormEventHandler} from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';


const ModalNota = ({state, toggle, modal, handleChange, handleSubmit, handleActions} :
                             {state:any, toggle:any, modal:any, handleChange:ChangeEventHandler,
                                 handleSubmit:FormEventHandler, handleActions:any }) => {
    return (
        <>
            <Modal isOpen={modal} toggle={toggle}  backdrop={"static"}>
                <ModalHeader toggle={toggle}>Actualizar/Borrar Datos</ModalHeader>
                <ModalBody>


                    <form onSubmit={handleSubmit} >
                        <div className={"form-floating"}>
                            <input placeholder={"Ingrese su Literal"} type={"text"} name={"literalAct"}
                                   defaultValue={state.literalAct}
                                   onChange={handleChange}  className={"form-control"} /><br/><br/>
                            <label htmlFor={"Nombre"}>Literal</label>
                        </div>

                        <div className={"form-floating"}>
                            <input placeholder={"Ingrese su Nota_desde"} type={"number"} name={"nota_desdeAct"}
                                   defaultValue={state.nota_desdeAct}
                                   onChange={handleChange} className={"form-control"}/><br/><br/>
                            <label htmlFor={"nota_desde"}>Nota_desde</label>
                        </div>

                        <div className={"form-floating"}>
                            <input placeholder={"Ingrese su Nota_hasta"} type={"number"} name={"nota_hastaAct"}
                                   defaultValue={state.nota_hastaAct}
                                   onChange={handleChange} className={"form-control"}/><br/><br/>
                            <label htmlFor={"nota_hasta"}>Nota_hasta</label>
                        </div>

                        <ModalFooter>

                            <button className={"btn btn-success"} onClick={handleActions} type={"submit"}
                                    value={"updateScore"}>Actualizar</button>
                            <Button className={"btn btn-danger"} onClick={handleActions} value={"deleteScore"}>Borrar</Button>
                            <Button color="secondary" onClick={toggle}>Cancelar</Button>

                        </ModalFooter>

                    </form>
                </ModalBody>

            </Modal>
        </>
    )
}

export default ModalNota;