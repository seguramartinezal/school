import React, {ChangeEventHandler, FormEventHandler} from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import Combobox from "react-widgets/Combobox";
import "react-widgets/styles.css";


const ModalCalificacion = ({state, toggle, modal, handleChange, handleSubmit, handleActions,} :
                              {state:any, toggle:any, modal:any, handleChange:ChangeEventHandler,
                                  handleSubmit:FormEventHandler, handleActions:any}) => {
    return (
        <>
            <Modal isOpen={modal} toggle={toggle}  backdrop={"static"}>
                <ModalHeader toggle={toggle}>Actualizar/Borrar Datos</ModalHeader>
                <ModalBody>

                    <form onSubmit={handleSubmit} >
                        <div className={"form-floating"}>
                            <input placeholder={"Ingrese su nombre"} type={"text"} readOnly
                                   defaultValue={state.nombre} onChange={handleChange}  className={"form-control"} /><br/>
                            <label htmlFor={"Nombre"}>Nombre</label>
                        </div>

                        <div className={"form-floating"}>
                            <input placeholder={"Ingrese su nombre"} type={"text"} readOnly
                                   defaultValue={state.grado} onChange={handleChange}  className={"form-control"} /><br/>
                            <label htmlFor={"Nombre"}>Grado</label>
                        </div>

                        <div className={"form-floating"}>
                            <input placeholder={"Ingrese su nombre"} type={"text"} readOnly
                                   defaultValue={state.materia} onChange={handleChange}  className={"form-control"} /><br/>
                            <label htmlFor={"Nombre"}>Materia</label>
                        </div>


                        <div className={"form-floating"}>
                            <input placeholder={"Ingrese su nombre"} type={"text"} name={"calificacion"}
                                   onChange={handleChange} defaultValue={state.calificacion}  className={"form-control"} /><br/>
                            <label htmlFor={"Nombre"}>Calificacion</label>
                        </div>

                        <ModalFooter>


                            {/*{state.calificacion>0 && state.calificacion != null  ?
                                <button className={"btn btn-success"} onClick={handleActions} type={"submit"} disabled
                                        value={"updateCalification"}>Guardar</button>
                             :
                                <button className={"btn btn-success"} onClick={handleActions} type={"submit"}
                                        value={"updateCalification"}>Guardar</button>}*/}


                            <button className={"btn btn-success"} onClick={handleActions} type={"submit"}
                                    disabled={state.literal != null }
                                    value={"updateCalification"}>Guardar</button>


                            <Button color="secondary" onClick={toggle}>Cancelar</Button>

                        </ModalFooter>

                    </form>
                </ModalBody>

            </Modal>


        </>
    )
}

export default ModalCalificacion;