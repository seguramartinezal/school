import React from "react";
import "../styles/RegistrarCalificacion.css";
import {AsyncInsertInterface} from "../interfaces/InsertInterface";
import {HandleMethods} from "../interfaces/HandleMethods";
import axios from "axios";
import {toast, ToastContainer} from "react-toastify";


class RegistrarCalificacion extends React.Component<any, any> implements AsyncInsertInterface, HandleMethods{

    constructor(props:any) {
        super(props);
        this.state = {
            literal: String,
            nota_desde: Number,
            nota_hasta: Number
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    url: string = "http://localhost:4000/";

    handleChange({target}: { target: any }): void {
        const {name, value} = target;
        this.setState(() => ({[name]:value}))
    }

    handleSubmit(e: any): void {
        e.preventDefault();
        e.target.reset();

        const payload = {
            literal: this.state.literal,
            nota_desde: this.state.nota_desde,
            nota_hasta: this.state.nota_hasta
        }

        this.asyncInsertMethod(payload);
        console.log(payload);
    }

    async asyncInsertMethod(data: any): Promise<void> {

        const res = await axios.post(this.url+"RegistrarNota", data)
        const payload = res.data;
        this.notificaciones(payload);

    }


    notificaciones(data:any){

        switch (data) {

            case true: {
                return toast.success("Nota registrada", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                })
            }
            default:
                break;
        }
    }

    render() {
        return (
            <div>

                <ToastContainer theme={"dark"}/>
                <h3 id={"tituloCalificacion"}>Registrar Nota</h3>

                <div className={"border"} id={"formCalificacion"}>

                    <div className={"calificacionFormulario"}>
                        <form autoComplete={"off"} onSubmit={this.handleSubmit}>

                            <div className={"form-floating"}>
                                <input placeholder={"Ingrese el literal"} type={"text"} name={"literal"} required
                                       onChange={this.handleChange} className={"form-control"} autoComplete={"off"}/><br/>
                                <label htmlFor={"Literal"}>Literal</label>
                            </div>

                            <div className={"form-floating"}>
                                <input placeholder={"Ingrese la nota_desde"} type={"number"} name={"nota_desde"} required
                                       onChange={this.handleChange} className={"form-control"} autoComplete={"off"}/><br/>
                                <label htmlFor={"Nota_desde"}>Nota_desde</label>
                            </div>

                            <div className={"form-floating"}>
                                <input placeholder={"Ingrese la nota_hasta"} type={"number"} name={"nota_hasta"} required
                                       onChange={this.handleChange} className={"form-control"} autoComplete={"off"}/><br/>
                                <label htmlFor={"Nota_hasta"}>Nota_hasta</label><br/>
                            </div>

                            <button type={"submit"} style={{position:"relative", left:"10em", bottom: "1.8em"}}
                                    className={"btn btn-primary"}>Guardar</button>
                        </form>
                    </div>

                </div>

            </div>
        );
    }
}

export default RegistrarCalificacion;