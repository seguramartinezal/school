import React from "react";
import {toast, ToastContainer} from "react-toastify";
import ModalEstudiante from "./ModalEstudiante";
import {AsyncGetUpdateDeleteInterface} from "../interfaces/AsyncGetUpdateDeleteInterface";
import {HandleMethods} from "../interfaces/HandleMethods";
import axios from "axios";
import ModalInscripcion from "./ModalInscripcion";



class InscripcionEstudiante extends React.Component<any, any>  implements AsyncGetUpdateDeleteInterface, HandleMethods {

    constructor(props: any) {
        super(props);

        this.state = {

            nombreEstudiante: String,
            gradoEstudiante: String,
            AllInscription: [],
            modal: false,
            obj: {},
            AllInscriptionCopy: [],
            ArrayGrades: [],
            Grado: {}

        }


        this.showModal = this.showModal.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.asyncGetMethod = this.asyncGetMethod.bind(this);
        this.asyncDeleteMethod = this.asyncDeleteMethod.bind(this);
        this.manageActions = this.manageActions.bind(this);
        this.asyncUpdateMethod = this.asyncUpdateMethod.bind(this);
        this.selectGrade = this.selectGrade.bind(this);
    }

    componentDidMount() {
        this.asyncGetMethod();
        this.asyncGetMethodGrade();
    }


    asyncDeleteMethod(data: any): Promise<void> {
        return Promise.resolve(undefined);
    }

    async asyncGetMethod(): Promise<void> {
        let dataArray: any[] = [];

        const res = await axios.get(this.url + "Inscripcion");
        for (let i = 0; i < res.data.length; i++) {
            const payload = {
                id: res.data[i].id,
                nombre: res.data[i].estudiante,
                grado: res.data[i].grado
            }

            dataArray.push(payload)
        }
        this.setState(() => ({AllInscription: dataArray}));
        this.setState(() => ({AllInscriptionCopy: dataArray}));
    }

    async asyncGetMethodGrade(): Promise<void> {
        let dataArray: any[] = [];

        const res = await axios.get(this.url + "Inscripciones");
        for (let i = 0; i < res.data.length; i++) {
            const payload = {
                id: res.data[i].id,
                nombre: res.data[i].grado
            }

            dataArray.push(payload)
        }
        this.setState(() => ({ArrayGrades: dataArray}));

    }

    async asyncUpdateMethod(data: any): Promise<void> {

        const payload = {
            id: data.data.id,
            nombre: data.data.nombre,
            grado: this.state.Grado.nombre
        }


        const actualizarGrado = {
            idGrado: this.state.Grado.id,
            idEstudiante: data.data.id
        }


        const res = await axios.put(this.url + "ActualizarGrado", {data: actualizarGrado});
        const jsonResponse = res.data;


        if (jsonResponse.success) {
            const studentArray = this.state.AllInscription;
            const newArray = studentArray.filter((item: any) => item.id != payload.id);
            newArray.push(payload);
            this.setState(() => ({AllInscription: newArray}))
            this.setState(() => ({AllstudentsCopy:newArray}));
        }

        this.notificacionActualizar(jsonResponse);

    }

    handleChange({target}: { target: any }): void {
    }

    handleSubmit(e: any): void {
        e.preventDefault();
    }

    loadDataMethod(data: any): void {
        this.setState(() => ({nombreEstudiante: data.nombre, gradoEstudiante:data.grado, obj: {data}}))

    }

    manageActions(e: any): void {
        this.showModal(e);

        // if(e.target.value === "deleteStudent"){
        //     this.asyncDeleteMethod(this.state.obj)
        // }

        if (e.target.value === "updateGrade") {
            this.asyncUpdateMethod(this.state.obj)
        }
    }

    showModal(data: any): void {
        this.loadDataMethod(data);
        this.setState(() => ({modal: !this.state.modal}));


    }

    selectGrade(e: any): void {

        this.setState(() => ({Grado: e}));

    }


    filterData = ({target}: {target:any}) => {
        const valor = target.value;
        const arrayCopy = this.state.AllInscriptionCopy;
        const ans = arrayCopy.filter((item:any) => valor.toUpperCase() === item.nombre.toUpperCase()
            || valor.toUpperCase() === item.grado.toUpperCase());
          this.setState(() => ({AllInscription:ans}));
          if(valor.length === 0 ){
              this.setState(() => ({AllInscription:arrayCopy}));
          }

    }


    url: string = "";

    private notificacionActualizar(data: any) {

        switch (data.success) {

            case true: {
                return toast.success("Registro actualizado", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                })
            }

            case false: {
                return toast.error("Error", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                })
            }

            default:
                break;

        }

    }


    render() {

        return (
            <>
                <div>
                    <ToastContainer theme={"dark"}/>
                    <h3 style={{position: "relative", left:"15em", top:"2em", width:"10em"}}>Consultar Inscripcion</h3>

                    <ModalInscripcion state={this.state} toggle={this.showModal} modal={this.state.modal}
                                      handleChange={this.handleChange} handleSubmit={this.handleSubmit}
                                      handleActions={this.manageActions} selectGrade={this.selectGrade}/>

                    <form className={"form"}>

                        <div className={"form-floating"} id={"BarraFiltro"}>
                            <input placeholder={"Ingrese su nombre"} type={"text"} name={"filtrarInscripcion"} onKeyUp={this.filterData}
                                   className={"form-control"} autoComplete={"off"}/><br/>
                            <label htmlFor={"Filtro"}><i>Filtrar inscripcion</i></label>
                        </div>
                    </form>

                    <table className={"table table-bordered"}>
                        <thead>
                        <tr>
                            <th scope={"col"}>#</th>
                            <th scope={"col"}>Nombre y Matricula</th>
                            <th scope={"col"}>Grado</th>


                        </tr>
                        </thead>

                        <tbody>

                        {this.state.AllInscription.map((item: any, index: number) =>

                            <tr key={index} onClick={() => this.showModal(item)}>
                                <th scope={"row"}>{item.id}</th>
                                <td>{item.nombre}</td>
                                {/*<td>{item.matricula}</td>*/}
                                <td>{item.grado}</td>

                            </tr>
                        )}

                        </tbody>

                    </table>

                </div>

            </>
        )
    }

}


export default InscripcionEstudiante;