import React from "react";
import {Container} from "reactstrap";


class Layout extends React.Component<any, any>{
    render() {
        return (
            <div>

               <Container>
                   {this.props.children}
               </Container>

            </div>
        );
    }
}


export default Layout;