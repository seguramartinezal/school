import React, {ChangeEventHandler, FormEventHandler} from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';


const ModalEstudiante = ({state, toggle, modal, handleChange, handleSubmit, handleActions} :
                             {state:any, toggle:any, modal:any, handleChange:ChangeEventHandler,
                                 handleSubmit:FormEventHandler, handleActions:any }) => {
    return (
        <>
            <Modal isOpen={modal} toggle={toggle}  backdrop={"static"}>
                <ModalHeader toggle={toggle}>Actualizar/Borrar Datos</ModalHeader>
                <ModalBody>


                    <form onSubmit={handleSubmit} >
                        <div className={"form-floating"}>
                            <input placeholder={"Ingrese su nombre"} type={"text"} name={"nombreEstudianteAct"} defaultValue={state.nombreEstudianteAct}
                                   onChange={handleChange}  className={"form-control"} /><br/><br/>
                            <label htmlFor={"Nombre"}>Nombre</label>
                        </div>

                        <div className={"form-floating"}>
                            <input placeholder={"Ingrese su apellido"} type={"text"} name={"apellidoEstudianteAct"} defaultValue={state.apellidoEstudianteAct}
                                   onChange={handleChange} className={"form-control"}/><br/><br/>
                            <label htmlFor={"Apellido"}>Apellido</label>
                        </div>

                        <div className={"form-floating"}>
                            <input placeholder={"Ingrese su matricula"} type={"text"} name={"matriculaEstudianteAct"} defaultValue={state.matriculaEstudianteAct}
                                   onChange={handleChange} className={"form-control"}/><br/><br/>
                            <label htmlFor={"Edad"}>Matricula</label>
                        </div>

                        <ModalFooter>

                            <button className={"btn btn-success"} onClick={handleActions} type={"submit"}
                                    value={"updateStudent"}>Actualizar</button>
                            <Button className={"btn btn-danger"} onClick={handleActions} value={"deleteStudent"}>Borrar</Button>
                            <Button color="secondary" onClick={toggle}>Cancelar</Button>

                        </ModalFooter>

                    </form>
                </ModalBody>

            </Modal>


        </>
    )
}

export default ModalEstudiante;