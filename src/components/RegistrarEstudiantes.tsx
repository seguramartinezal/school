import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../styles/RegistrarEstudiantes.css";
import axios from "axios";
import {AsyncInsertInterface} from "../interfaces/InsertInterface";
import {HandleMethods} from "../interfaces/HandleMethods";
import {ToastContainer, toast} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';


class RegistrarEstudiantes extends React.Component<any, any> implements AsyncInsertInterface, HandleMethods{

    constructor(props:any) {
        super(props);
        this.state = {
            nombreEstudiante: String,
            apellidoEstudiante: String,
            matriculaEstudiante: Number,
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    url: string = "http://localhost:4000/";

    handleChange({target}: { target: any }): void {
        const {name, value} = target;
        this.setState(() => ({[name]:value}))
    }

     handleSubmit(e: any): void {
        e.preventDefault();
        e.target.reset();

        const payload = {
            nombre: this.state.nombreEstudiante,
            apellido: this.state.apellidoEstudiante,
            matricula: this.state.matriculaEstudiante
        }

        this.asyncInsertMethod(payload);
    }

    async asyncInsertMethod(data: any): Promise<void> {

        const res = await axios.post(this.url+"RegistrarEstudiantes", data)

        const payload = res.data;
        this.notificaciones(payload);
    }

    notificaciones(data:any){

        switch (data) {

            case true: {
                return toast.success("Estudiante registrado", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                })
            }
            case false:{
                return toast.info("Matricula existente", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                })

            }
                break;
        }
    }


    render() {
        return (

            <>
                <ToastContainer theme={"dark"}/>

                <h3 id={"titulo"}>Registrar Estudiante</h3>

                <div className={"border"} id={"form"}>

                    <div className={"datosFormulario"}>
                        <form onSubmit={this.handleSubmit} autoComplete={"off"}>

                            <div className={"form-floating"}>
                                <input placeholder={"Ingrese su nombre"} type={"text"} name={"nombreEstudiante"} required
                                       onChange={this.handleChange} className={"form-control"} autoComplete={"off"}/><br/>
                                <label htmlFor={"Nombre"}>Nombre</label>
                            </div>

                            <div className={"form-floating"}>
                                <input placeholder={"Ingrese su apellido"} type={"text"} name={"apellidoEstudiante"} required
                                       onChange={this.handleChange} className={"form-control"} autoComplete={"off"}/><br/>
                                <label htmlFor={"Apellido"}>Apellido</label>
                            </div>

                            <div className={"form-floating"}>
                                <input placeholder={"Ingrese su matricula"} type={"text"} name={"matriculaEstudiante"} required
                                       onChange={this.handleChange} className={"form-control"} autoComplete={"off"}/><br/><br/>
                                <label htmlFor={"Matricula"} >Matricula</label>
                            </div>

                            <button type={"submit"} style={{position:"relative", left:"10em", bottom: "1.7em"}}
                                    className={"btn btn-primary"}>Guardar</button>
                        </form>
                    </div>

                </div>

            </>

        )
    }


}


export default RegistrarEstudiantes;