import React, {useState} from "react";
import Combobox from "react-widgets/Combobox";
import "react-widgets/styles.css";
import {AsyncGetUpdateDeleteInterface} from "../interfaces/AsyncGetUpdateDeleteInterface";
import axios from "axios";
import {HandleMethods} from "../interfaces/HandleMethods";
import {toast, ToastContainer} from "react-toastify";
import ModalAsistencia from "./ModalAsistencia";



class HistorialAsistencia extends React.Component<any, any> implements AsyncGetUpdateDeleteInterface, HandleMethods {


    constructor(props:any) {
        super(props);
        this.state = {
            GradesArray: [],
            SubjectsArray: [],
            modal: false,
            objGrade: {},
            objSubject: {},
            AllItem: [],
            AllItemCopy: [],
            nombre: String,
            grado: String,
            materia: String,
            obj:{},
            asistencia: ["Si", "No"],
            validacionAsistencia: String,
            fecha: Date
        }


        this.showModal = this.showModal.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.asyncGetMethod = this.asyncGetMethod.bind(this);
        this.asyncDeleteMethod = this.asyncDeleteMethod.bind(this);
        this.manageActions = this.manageActions.bind(this);
        this.asyncUpdateMethod = this.asyncUpdateMethod.bind(this);
        this.handleSelectSubject = this.handleSelectSubject.bind(this);
        this.handleSelectGrade = this.handleSelectGrade.bind(this);
        this.handleDate = this.handleDate.bind(this);
    }

    componentDidMount() {
        this.asyncGetMethodGrades();
        this.asyncGetMethodSubjects();
    }

    handleChange({target}: { target: any }): void {
        this.setState(() => ({[target.name]:target.value}))
    }

    handleSubmit(e: any): void {
        e.preventDefault();
    }

    url: string = "";

    asyncDeleteMethod(data: any): Promise<void> {
        return Promise.resolve(undefined);
    }

    asyncGetMethod(): Promise<void> {
        return Promise.resolve(undefined);
    }


    async asyncGetMethodSubjects(): Promise<void> {
        let dataArray: any[] = [];

        const res = await axios.get(this.url + "/HistorialAsistencia/Materia");
        for (let i = 0; i < res.data.length; i++) {
            const payload = {
                id: res.data[i].Id_Materia,
                nombre: res.data[i].Nombre
            }

            dataArray.push(payload)
        }
        this.setState(() => ({SubjectsArray: dataArray}));
    }

    async asyncGetMethodGrades(): Promise<void> {
        let dataArray: any[] = [];

        const res = await axios.get(this.url + "/HistorialAsistencia/Grado");
        for (let i = 0; i < res.data.length; i++) {
            const payload = {
                id: res.data[i].ID_Grado,
                nombre: res.data[i].Nombre
            }

            dataArray.push(payload)
        }
        this.setState(() => ({GradesArray: dataArray}));
    }


    async asyncUpdateMethod(data: any): Promise<void> {

        const payload = {
            id: data.id,
            nombre: data.nombre,
            grado: data.grado,
            materia: data.materia,
            fecha: data.fecha,
            asistencia: this.state.validacionAsistencia

        }

        const ArrayAll = this.state.AllItem;

        const newArray = ArrayAll.filter((item: any) => item.id !== payload.id);
        this.setState(() => ({AllItem: newArray}))
        newArray.push(payload);
        this.setState(() => ({AllItem: newArray}))


        const updateAsistencia = {
            id: payload.id,
            fecha: payload.fecha,
            asistencia: payload.asistencia === "Si" ? true : false
        }

        const res = await axios.put(this.url + "ActualizarAsistencia", {data: updateAsistencia});
        if(res.data){
            this.notificacionActualizar(res.data)
            this.showModal('');
        }


    }

    loadDataMethod(data: any): void {

        this.setState(() => ({nombre: data.nombre, grado:data.grado, materia:data.materia, obj:data}))
    }

    manageActions(e: any): void {
        if (e.target.value === "updateCalification") {
            this.asyncUpdateMethod(this.state.obj)
        }
    }

    showModal(data: any): void {
        this.loadDataMethod(data);
        this.setState(() => ({modal: !this.state.modal}))
    }

    handleSelectGrade = (e:any) => {
        this.setState(() => ({objGrade:e}))

    }

    handleSelectSubject = (e:any) => {
        this.setState(() => ({objSubject:e}))
    }

    handleSearch = async () => {
        let dataArray: any[] = [];
        const materia = this.state.objSubject.nombre;
        const grado = this.state.objGrade.nombre;
        const fecha = this.state.fecha;

        const regex = /^\d{4}-\d{2}-\d{2}$/;
        if (fecha.match(regex) === null) {
            return alert("Ingresa una fecha valida");
        }else{
            const res = await axios.get(this.url + "/HistorialAsistencia", {params: {nombreMateria: materia, idGrado:grado, fecha:fecha}});
            for (let i = 0; i < res.data.length; i++) {
                const fue = res.data[i].asistencia;
                let literal : string;
                if(fue != null){
                    literal = fue === true ? "Si" : "No"
                } else {
                    literal = ''
                }
                const payload = {
                    id: res.data[i].id,
                    nombre: res.data[i].estudiante,
                    grado: res.data[i].grado,
                    materia: res.data[i].materia,
                    asistencia: literal,
                    fecha: new Date().toISOString().split('T')[0].toString()
                }

                dataArray.push(payload)

            }

            this.setState(() => ({AllItem: dataArray}));
            this.setState(() => ({AllItemCopy: dataArray}));
        }

    }

    private notificacionActualizar(data: any) {

        switch (data.success) {

            case true: {
                return toast.success("Registro actualizado", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                })
            }

            case false: {
                return toast.error("Error", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                })
            }

            default:
                break;

        }

    }

    handleAsistencia = (data:any) => {
        this.setState(() => ({validacionAsistencia:data}));


    }

    handleDate({target}: {target:any}){
        const valor = target.value;
        this.setState(() => ({fecha: valor}));
    }


    render() {
        return (
            <div>
                <ToastContainer theme={'dark'}/>

                <h3 style={{position: "relative", left:"15em", top:"2em", width: "12em"}}>Historial de Asistencias</h3>
                <ModalAsistencia state={this.state} toggle={this.showModal} modal={this.state.modal}
                                 handleChange={this.handleChange} handleSubmit={this.handleSubmit}
                                 handleActions={this.manageActions} handleAsistencia={this.handleAsistencia}/>

                <div style={{position: "relative", left:"10em", top:"3em", width: "10em"}}>
                <h4 style={{position:"relative", top:"3.3em"}}>Grado</h4>
                <h4 style={{position:"relative", top:"1.8em", left:"8.7em", width:"10px"}}>Materia</h4>

                <Combobox data={this.state.GradesArray} textField='nombre'  defaultValue={"Grado"}
                          onSelect={e => this.handleSelectGrade(e)} style={{width: "10em",
                    position:"relative", top:"2.47em"}}/>

                <Combobox data={this.state.SubjectsArray} textField='nombre' defaultValue={"Materia"}
                          onSelect={e => this.handleSelectSubject(e)} style={{width: "16em", position:"relative",
                    left:"13em"}} />


                <form onSubmit={this.handleSubmit}>
                <div className={"form-floating"} style={{position: "relative", width:"13em", bottom: "3em", left:"30em" }}>
                    <input type={"text"} placeholder={"Ingrese la fecha"} onKeyUp={this.handleDate} required
                           className={"form-control"}/><br/>
                    <label htmlFor={"Filtro"}><i>Filtrar fecha</i></label>
                </div>
                </form>

                <button className={"btn btn-primary"} onClick={this.handleSearch}
                        style={{position:"relative", left:"44em", bottom:"7.5em"}}>Buscar</button> </div>


                <table className={"table table-bordered"} style={{position:"relative", top: "1em"}}>
                    <thead>
                    <tr>
                        <th scope={"col"}>#</th>
                        <th scope={"col"}>Nombre</th>
                        <th scope={"col"}>Grado</th>
                        <th scope={"col"}>Materia</th>
                        <th scope={"col"}>Fecha</th>
                        <th scope={"col"}>Asistencia</th>

                    </tr>
                    </thead>

                    <tbody>


                    {this.state.AllItem.map((item:any, index:number) => (
                        <tr key={index}>
                            <th scope={"row"}>{item.id} </th>
                            <td>{item.nombre}</td>
                            <td>{item.grado}</td>
                            <td>{item.materia}</td>
                            <td>{item.fecha}</td>
                            <td>{item.asistencia}</td>


                        </tr>


                    ))}

                    </tbody>

                </table>


            </div>
        );
    }


}


export default HistorialAsistencia;

