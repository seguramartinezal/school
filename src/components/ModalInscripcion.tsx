import React, {ChangeEventHandler, FormEventHandler} from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import Combobox from "react-widgets/Combobox";
import "react-widgets/styles.css";


const ModalInscripcion = ({state, toggle, modal, handleChange, handleSubmit, handleActions, selectGrade} :
                        {state:any, toggle:any, modal:any, handleChange:ChangeEventHandler,
                            handleSubmit:FormEventHandler, handleActions:any, selectGrade:any }) => {
    return (
        <>
            <Modal isOpen={modal} toggle={toggle}  backdrop={"static"}>
                <ModalHeader toggle={toggle}>Actualizar/Borrar Datos</ModalHeader>
                <ModalBody>

                    <form onSubmit={handleSubmit} >
                        <div className={"form-floating"}>
                            <input placeholder={"Ingrese su nombre"} type={"text"} name={"Grado"} defaultValue={state.nombreEstudiante}
                                   onChange={handleChange}  className={"form-control"} /><br/>
                            <label htmlFor={"Nombre"}>Nombre</label>
                        </div>

                        <div className={"form-floating"}>
                            <input placeholder={"Ingrese su nombre"} type={"text"} name={"Grado"} defaultValue={state.gradoEstudiante}
                                   onChange={handleChange}  className={"form-control"} readOnly/><br/>
                            <label htmlFor={"Nombre"}>Grado</label>
                        </div>


                        {state.gradoEstudiante != null ? <>

                            <ModalFooter>

                                <button className={"btn btn-success"} onClick={handleActions} type={"submit"}
                                        value={"updateGrade"} disabled={state.gradoEstudiante != null}>Agregar curso</button>
                                <Button color="secondary" onClick={toggle}>Cancelar</Button>

                            </ModalFooter>


                        </> : <>

                            <Combobox style={{width: "16em"}} onSelect={e => selectGrade(e)} defaultValue={"Selecciona una opcion"}
                                      data={state.ArrayGrades} textField='nombre' /><br/>

                            <ModalFooter>

                                <button className={"btn btn-success"} onClick={handleActions} type={"submit"}
                                        value={"updateGrade"} disabled={state.gradoEstudiante != null}>Agregar curso</button>
                                <Button color="secondary" onClick={toggle}>Cancelar</Button>

                            </ModalFooter>


                        </>}



                        {/*<Combobox style={{width: "16em"}} onSelect={e => selectGrade(e)} defaultValue={"Selecciona una opcion"}
                                  data={state.ArrayGrades} textField='nombre' /><br/>

                        <ModalFooter>

                            <button className={"btn btn-success"} onClick={handleActions} type={"submit"}
                                    value={"updateGrade"} disabled={state.gradoEstudiante != null}>Agregar curso</button>
                            <Button color="secondary" onClick={toggle}>Cancelar</Button>

                        </ModalFooter>*/}

                    </form>
                </ModalBody>

            </Modal>


        </>
    )
}

export default ModalInscripcion;