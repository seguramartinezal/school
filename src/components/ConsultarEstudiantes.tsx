import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../styles/ConsultarEstudiantes.css";
import {AsyncGetUpdateDeleteInterface} from "../interfaces/AsyncGetUpdateDeleteInterface";
import {HandleMethods} from "../interfaces/HandleMethods";
import axios from "axios";
import ModalEstudiante from "./ModalEstudiante";
import {toast, ToastContainer} from "react-toastify";


class ConsultarEstudiantes extends React.Component<any, any> implements AsyncGetUpdateDeleteInterface, HandleMethods{

    constructor(props:any) {
        super(props);
        this.state = {
            nombreEstudianteAct: String,
            apellidoEstudianteAct: String,
            matriculaEstudianteAct: Number,
            Allstudents: [],
            modal: false,
            obj: {},
            ////////////////////
            AllstudentsCopy: []

        }

        this.showModal = this.showModal.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.asyncGetMethod = this.asyncGetMethod.bind(this);
        this.asyncDeleteMethod = this.asyncDeleteMethod.bind(this);
        this.manageActions = this.manageActions.bind(this);
        this.asyncUpdateMethod = this.asyncUpdateMethod.bind(this);
    }

    url: string = "http://localhost:4000/";

   async componentDidMount() {
       await this.asyncGetMethod();
    }

    handleChange({target}: { target: any }): void {
        const name = target.name;
        const value = target.value;
        this.setState(() => ({[name]:value}));
    }

    handleSubmit(e: any): void {
        e.preventDefault();
        e.target.reset();
    }


    async asyncGetMethod(): Promise<void> {

       let dataArray: any[] = [];

        const res = await axios.get(this.url+"ConsultarEstudiantes");
        for (let i = 0; i < res.data.length; i++) {
            const payload = {
                id: res.data[i].Id_estudiante,
                nombre: res.data[i].Nombre,
                apellido: res.data[i].Apellido,
                matricula: res.data[i].Matricula,
            }

            dataArray.push(payload)
        }
        this.setState(() => ({Allstudents:dataArray}));
        this.setState(() => ({AllstudentsCopy:dataArray}));

    }

    async asyncDeleteMethod(data: any): Promise<void> {
        const res = await axios.delete(this.url+"ConsultarEstudiantes", {data: {data: data.data.id}});
        const jsonResponse = res.data;
        if(jsonResponse.success){
            const studentArray = this.state.Allstudents;
            const newArray = studentArray.filter((item:any) => item.id != data.data.id);
            this.setState(() => ({Allstudents:newArray}));
            this.setState(() => ({AllstudentsCopy:newArray}));
        }

        this.notificacionBorrar(jsonResponse);
    }


    async asyncUpdateMethod(data: any): Promise<void> {

        const payload = {
            id: data.data.id,
            nombre: this.state.nombreEstudianteAct,
            apellido: this.state.apellidoEstudianteAct,
            matricula: this.state.matriculaEstudianteAct
        }

        const res = await axios.put(this.url+"ConsultarEstudiantes", {data: payload});
        const jsonResponse = res.data;

        if(jsonResponse.success){
            const studentArray = this.state.Allstudents;
            console.log(studentArray);
            const newArray = studentArray.filter((item:any) => item.id != payload.id);
             newArray.push(payload);
            this.setState(() => ({Allstudents:newArray}))
            this.setState(() => ({AllstudentsCopy:newArray}));
        }

        this.notificacionActualizar(jsonResponse);

    }

    filterData = ({target}: {target:any}) => {
       const valor = target.value;
       const arrayCopy = this.state.AllstudentsCopy;
       const ans = arrayCopy.filter((item:any) => valor.toUpperCase() === item.nombre.toUpperCase()
           || valor.toUpperCase() === item.apellido.toUpperCase() || valor === String(item.matricula).toUpperCase());
       this.setState(() => ({Allstudents:ans}));
       if(valor.length == 0 ){
           this.setState(() => ({Allstudents:arrayCopy}));
       }

    }

    manageActions(e:any): void {
       this.showModal(e);

       if(e.target.value === "deleteStudent"){
          this.asyncDeleteMethod(this.state.obj)
       }

       if(e.target.value === "updateStudent"){
           this.asyncUpdateMethod(this.state.obj)
       }
    }

    loadDataMethod(data:any): void {

        this.setState(() => ({nombreEstudianteAct:data.nombre, apellidoEstudianteAct:data.apellido,
            matriculaEstudianteAct:data.matricula, obj:{data}}))

    }

    showModal(data: any): void {
        this.loadDataMethod(data);
        this.setState(() => ({modal: !this.state.modal}))
    }

    notificacionActualizar(data:any){

        switch (data.success) {

            case true: {
                return toast.success("Registro actualizado", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                })
            }

            case false:{
                return toast.error("Error", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                })
            }

            default:
               break;

        }
    }

    notificacionBorrar(data:any){

        switch (data.success) {

            case true: {
                return toast.success("Registro borrado", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                })
            }

            case false:{
                return toast.error("Error", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                })
            }

            default:
                break;

        }
    }


    render() {
        return (
            <div>
                <ToastContainer theme={"dark"}/>

                <h3 style={{position: "relative", left:"15em", top:"2em", width: "10em"}}>Consultar Estudiante</h3>

                <ModalEstudiante state={this.state} toggle={this.showModal} modal={this.state.modal}
                                 handleChange={this.handleChange} handleSubmit={this.handleSubmit}
                                 handleActions={this.manageActions}/>

                <form className={"form"}>

                <div className={"form-floating"} id={"BarraFiltro"}>
                    <input placeholder={"Ingrese su nombre"} type={"text"} name={"filtrarEstudiante"} onKeyUp={this.filterData}
                           className={"form-control"} autoComplete={"off"}/><br/>
                    <label htmlFor={"Filtro"}><i>Filtrar estudiante</i></label>
                </div>
                </form>

                <table className={"table table-bordered"}>
                    <thead>
                    <tr>
                        <th scope={"col"}>#</th>
                        <th scope={"col"}>Nombre</th>
                        <th scope={"col"}>Apellido</th>
                        <th scope={"col"}>Matricula</th>

                    </tr>
                    </thead>

                    <tbody>

                       {this.state.Allstudents.map((item:any, index:number) =>

                    <tr key={index} onClick={() => this.showModal(item)}>
                        <th scope={"row"}>{item.id}</th>
                        <td>{item.nombre}</td>
                        <td>{item.apellido}</td>
                        <td>{item.matricula}</td>
                    </tr>

                )}

                    </tbody>

                </table>

            </div>
        );
    }

}



export default ConsultarEstudiantes;