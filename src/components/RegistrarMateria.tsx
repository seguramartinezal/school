import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../styles/RegistrarMateria.css";
import {AsyncInsertInterface} from "../interfaces/InsertInterface";
import {HandleMethods} from "../interfaces/HandleMethods";
import axios from "axios";
import {toast, ToastContainer} from "react-toastify";


class RegistrarMateria extends React.Component<any, any> implements AsyncInsertInterface, HandleMethods{

    constructor(props:any) {
        super(props);
        this.state = {
            nombreMateria: String
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    url: string = "http://localhost:4000/";

    handleChange({target}: { target: any }): void {
        const {name, value} = target;
        this.setState(() => ({[name]:value}))
    }

    handleSubmit(e: any): void {
        e.preventDefault();
        e.target.reset();

        const payload = {
            nombre: this.state.nombreMateria,
        }

        this.asyncInsertMethod(payload);
    }

    async asyncInsertMethod(data: any): Promise<void> {

        const res = await axios.post(this.url+"RegistrarMateria", data);
        const payload = res.data;
        this.notificaciones(payload);
    }

    notificaciones(data:any){

        switch (data) {

            case true: {
                return toast.success("Materia registrada", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                })
            }
            default:
                break;
        }
    }


    render() {
        return (
            <div>
                <ToastContainer theme={"dark"}/>

                <h3 id={"tituloMateria"}>Registrar Materia</h3>

                <div className={"border"} id={"formMateria"}>

                    <div className={"MateriaFormulario"}>
                        <form autoComplete={"off"} onSubmit={this.handleSubmit}>

                            <div className={"form-floating"}>
                                <input placeholder={"Ingrese el nombre"} type={"text"} name={"nombreMateria"} required
                                      onChange={this.handleChange} className={"form-control"} autoComplete={"off"}/><br/>
                                <label htmlFor={"Nombre"}>Nombre</label>
                            </div><br/>

                            <button type={"submit"} style={{position:"relative", left:"10em", bottom: "1.7em"}}
                                    className={"btn btn-primary"}>Guardar</button>
                        </form>
                    </div>

                </div>
            </div>
        );
    }


}

export default RegistrarMateria;