import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../styles/ConsultarCalificacion.css";
import {AsyncGetUpdateDeleteInterface} from "../interfaces/AsyncGetUpdateDeleteInterface";
import {HandleMethods} from "../interfaces/HandleMethods";
import axios from "axios";
import ModalNota from "./ModalNota";
import {toast, ToastContainer} from "react-toastify";


class ConsultarCalificacion extends React.Component<any, any> implements AsyncGetUpdateDeleteInterface, HandleMethods{

    constructor(props:any) {
        super(props);
        this.state = {
            literalAct: String,
            nota_desdeAct: Number,
            nota_hastaAct: Number,
            AllScores: [],
            modal: false,
            obj: {},
            AllScoresCopy: []

        }

        this.showModal = this.showModal.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.asyncGetMethod = this.asyncGetMethod.bind(this);
        this.asyncDeleteMethod = this.asyncDeleteMethod.bind(this);
        this.manageActions = this.manageActions.bind(this);
        this.asyncUpdateMethod = this.asyncUpdateMethod.bind(this);
    }

    url: string = "http://localhost:4000/";

    async componentDidMount() {
        await this.asyncGetMethod();
    }

    handleChange({target}: { target: any }): void {
        const {name, value} = target;
        this.setState(() => ({[name]:value}));
    }

    handleSubmit(e: any): void {
        e.preventDefault();
        e.target.reset();
    }


    async asyncGetMethod(): Promise<void> {
        let dataArray: any[] = [];
        const res = await axios.get(this.url+"ConsultarNota");
        for (let i = 0; i < res.data.length; i++) {
            const payload = {
                id: res.data[i].Id_Nota,
                literal: res.data[i].Literal,
                rango_desde: res.data[i].Rango_desde,
                rango_hasta: res.data[i].Rango_hasta,
            }

            dataArray.push(payload);

            this.setState(() => ({AllScores:dataArray}));
            this.setState(() => ({AllScoresCopy:dataArray}));
        }
    }

    async asyncDeleteMethod(data: any): Promise<void> {
        const res = await axios.delete(this.url+"ConsultarNota", {data: {data: data.data.id}});
        const jsonResponse = res.data;
        if(jsonResponse.success){
            const studentArray = this.state.AllScores;
            const newArray = studentArray.filter((item:any) => item.id != data.data.id);
            this.setState(() => ({AllScores:newArray}))
            this.setState(() => ({AllScoresCopy:newArray}));
        }

        this.notificacionBorrar(jsonResponse);
    }


    async asyncUpdateMethod(data: any): Promise<void> {

        const payload = {
            id: data.data.id,
            literal: this.state.literalAct,
            rango_desde: this.state.nota_desdeAct,
            rango_hasta: this.state.nota_hastaAct
        }

        const res = await axios.put(this.url+"ConsultarNota", {data: payload});

        const jsonResponse = res.data;

        if(jsonResponse.success){
            const scoreArray = this.state.AllScores;
            console.log(scoreArray);
            const newArray = scoreArray.filter((item:any) => item.id != payload.id);
            newArray.push(payload);
            this.setState(() => ({AllScores:newArray}))
            this.setState(() => ({AllScoresCopy:newArray}));
        }

        this.notificacionActualizar(jsonResponse);

    }

    filterData = ({target}: {target:any}) => {
        const valor = target.value;

        const arrayCopy = this.state.AllScoresCopy;
        const ans = arrayCopy.filter((item:any) => String(valor) === String(item.rango_hasta) ||
            valor.toUpperCase() === item.literal.toUpperCase() || String(valor) === String(item.rango_desde));

        this.setState(() => ({AllScores:ans}));
        if(valor.length == 0 ){
            this.setState(() => ({AllScores:arrayCopy}));
        }

    }

    manageActions(e:any): void {
        this.showModal(e);

        if(e.target.value === "deleteScore"){
            this.asyncDeleteMethod(this.state.obj)
        }

        if(e.target.value === "updateScore"){
            this.asyncUpdateMethod(this.state.obj)
        }

    }

    loadDataMethod(data:any): void {
        this.setState(() => ({literalAct:data.literal, nota_desdeAct:data.rango_desde,
            nota_hastaAct:data.rango_hasta, obj:{data}}))
    }

    showModal(data: any): void {
        this.loadDataMethod(data);
        this.setState(() => ({modal: !this.state.modal}))
    }

    notificacionActualizar(data:any){

        switch (data.success) {

            case true: {
                return toast.success("Registro actualizado", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                })
            }

            case false:{
                return toast.error("Error", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                })
            }

            default:
                break;

        }
    }

    notificacionBorrar(data:any){

        switch (data.success) {

            case true: {
                return toast.success("Registro borrado", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                })
            }

            case false:{
                return toast.error("Error", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                })
            }

            default:
                break;

        }
    }


    render() {
        return (
            <div>
                <ToastContainer theme={"dark"}/>
                <ModalNota state={this.state} toggle={this.showModal} modal={this.state.modal}
                                 handleChange={this.handleChange} handleSubmit={this.handleSubmit}
                                 handleActions={this.manageActions}/>

                <form className={"formCalificacion"}>

                    <div className={"form-floating"} id={"BarraFiltroCalificacion"}>
                        <input type={"text"} name={"filtrarCalificacion"} placeholder={"Ingrese la calificacion"} onKeyUp={this.filterData}
                               className={"form-control"} autoComplete={"off"}/><br/>
                        <label htmlFor={"Filtro"}><i>Filtrar calificacion</i></label>
                    </div>
                </form>

                <table className={"table table-bordered"}>
                    <thead>
                    <tr>
                        <th scope={"col"}>#</th>
                        <th scope={"col"}>Nombre</th>
                        <th scope={"col"}>Nota_desde</th>
                        <th scope={"col"}>Nota_hasta</th>

                    </tr>
                    </thead>

                    <tbody>

                    {this.state.AllScores.map((item:any, index:number) =>

                        <tr key={index} onClick={() => this.showModal(item)}>
                            <th scope={"row"}>{item.id}</th>
                            <td>{item.literal}</td>
                            <td>{item.rango_desde}</td>
                            <td>{item.rango_hasta}</td>
                        </tr>

                    )}


                    </tbody>

                </table>

            </div>
        );
    }
}

export default ConsultarCalificacion;