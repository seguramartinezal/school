import React from 'react';
import {Route, BrowserRouter as Router, Routes} from "react-router-dom";
import Layout from "./components/Layout";
import Inicio from "./components/Inicio";
import ConsultarEstudiantes from "./components/ConsultarEstudiantes";
import RegistrarEstudiantes from "./components/RegistrarEstudiantes";
import NavMenu from "./components/NavMenu";
import RegistrarMateria from "./components/RegistrarMateria";
import ConsultarMateria from "./components/ConsultarMateria";
import RegistrarCurso from "./components/RegistrarCurso";
import ConsultarCurso from "./components/ConsultarCurso";
import RegistrarCalificacion from "./components/RegistrarCalificacion";
import ConsultarCalificacion from "./components/ConsultarCalificacion";
import Calificacion from "./components/Calificacion";
import InscripcionEstudiante from "./components/InscripcionEstudiante";
import RegistroAsisencia from "./components/RegistroAsistencia";
import HistorialAsistencia from "./components/HistorialAsistencia";


class App extends React.Component<any, any>{

  render() {
    return (
        <div>

            <Layout>
                <Router>
                    <NavMenu/>
                    <Routes>
                        <Route path={'/'} element={<Inicio/>}/>
                        <Route path={'/RegistrarEstudiantes'} element={<RegistrarEstudiantes/>}/>
                        <Route path={'/ConsultarEstudiantes'} element={<ConsultarEstudiantes/>}/>
                     {/*   <Route path={'/RegistrarMateria'} element={<RegistrarMateria/>}/>
                        <Route path={'/ConsultarMateria'} element={<ConsultarMateria/>}/>*/}
                        <Route path={'/RegistrarGrado'} element={<RegistrarCurso/>}/>
                        <Route path={'/ConsultarGrado'} element={<ConsultarCurso/>}/>
                       {/* <Route path={'/RegistrarNota'} element={<RegistrarCalificacion/>}/>
                        <Route path={'/ConsultarNota'} element={<ConsultarCalificacion/>}/>*/}
                        <Route path={'/Calificacion'} element={<Calificacion/>}/>
                        <Route path={'/Inscripcion'} element={<InscripcionEstudiante/>}/>
                        <Route path={'/Asistencia'} element={<RegistroAsisencia/>}/>
                        <Route path={'/HistorialAsistencia'} element={<HistorialAsistencia/>}/>

                    </Routes>

                </Router>

            </Layout>
            
        </div>
    );
  }
}

export default App;
